/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.midterm02;

/**
 *
 * @author ripgg
 */
public class TestGlasses {

    public static void main(String[] args) {
        Glasses glasses1 = new Glasses("A01J", "Rectangle", "Glasses", "C", '1');
        System.out.println("Name:" + glasses1.name + " Shape: " + glasses1.shape + " Feature:" + glasses1.feature + " Model: " + glasses1.model + " Efficacy: " + glasses1.efficacy + " Price: " + glasses1.price("Round"));
        glasses1.setG("F05M", "Round", "Glasses", "D", '0');
        System.out.println("Name:" + glasses1.name + " Shape: " + glasses1.shape + " Feature:" + glasses1.feature + " Model: " + glasses1.model + " Efficacy: " + glasses1.efficacy + " Price: " + glasses1.price("Pilot"));
        System.out.println();
        Sunglasses sunglasses1 = new Sunglasses("B12B", "Pilot", "Sunglasses", "E", '1');
        System.out.println("Name:" + sunglasses1.name + " Shape: " + sunglasses1.shape + " Feature:" + sunglasses1.feature + " Model: " + sunglasses1.model + " Efficacy: " + sunglasses1.efficacy + " Price: " + sunglasses1.price("Round"));
        sunglasses1.setG("F05M", "Round", "Glasses", "D", '0');
        System.out.println("Name:" + sunglasses1.name + " Shape: " + sunglasses1.shape + " Feature:" + sunglasses1.feature + " Model: " + sunglasses1.model + " Efficacy: " + sunglasses1.efficacy + " Price: " + sunglasses1.price("Pilot"));
        System.out.println();
        Bluelightfilter bluelightfilter1 = new Bluelightfilter("M21P", "Square", "Bluelightfilter", "D", '1');
        System.out.println("Name:" + bluelightfilter1.name + " Shape: " + bluelightfilter1.shape + " Feature:" + bluelightfilter1.feature + " Model: " + bluelightfilter1.model + " Efficacy: " + bluelightfilter1.efficacy + " Price: " + bluelightfilter1.price("Rectangle"));
        bluelightfilter1.setG("F05M", "Round", "Glasses", "D", '0');
        System.out.println("Name:" + bluelightfilter1.name + " Shape: " + bluelightfilter1.shape + " Feature:" + bluelightfilter1.feature + " Model: " + bluelightfilter1.model + " Efficacy: " +bluelightfilter1.efficacy + " Price: " + bluelightfilter1.price("Square"));
    }

}
