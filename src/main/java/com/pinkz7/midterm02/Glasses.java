/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.midterm02;

/**
 *
 * @author ripgg
 */
public class Glasses {
    protected String name;//ชื่อ
    protected String shape;//ทรงแว่น
    protected String feature;//คุณสมบัติ
    protected String model;//รุ่น
    protected int efficacy=50;//ประสิทธิภาพ
    protected char choose;//เลือก
    protected int price=150;//ราคา
    
    Glasses(String name, String shape, String feature,String model,char choose) {
        this.name = name;
        this.shape = shape;
        this.feature = feature;
        this.model = model;
        
        switch (model) {//เปลี่ยนรุ่นประสิทธิภาพเปลี่ยน
            case "B":
                efficacy = 60;
                break;
            case "C":
                efficacy=70;
                break;
            case "D":
                efficacy=80;
                break;
            case "E":
                efficacy=90;
                break;
            default:
        }
    }
    public String getN() {
        return name;
    }

    public String getS() {
        return shape;
    }

    public String getF() {
        return feature;
    }

    public String getM() {
        return model;
    }

    public int setG(String name, String shape, String feature, String model, char choose) {
        if (choose == '1') {
            this.name = getN();
            this.shape = getS();
            this.feature = getF();
            this.model = model;
            
        } else if (choose == '0') {
            this.name = name;
            this.shape = shape;
            this.feature = feature;
            this.model = model;
        } 
        switch (model) {
            case "B":
                efficacy = 55;
                break;
            case "C":
                efficacy=65;
                break;
            case "D":
                efficacy=75;
                break;
            case "E":
                efficacy=85;
                break;
            default:
        }
        return efficacy;
    }
    
    public int price(){//ราคา
        return price;
    }
    
    public int price(String shape){//ราคาที่เมื่อเปลี่ยนทรงแว่น
        if(shape=="Round"){
            price+=50;
        }else if(shape=="Rectangle"){
            price+=75;
        }else if(shape=="Square"){
            price+=75;
        }else if(shape=="Sport"){
            price+=100;
        }else if(shape=="Pilot"){
            price+=50;
        }
        return price;
    }
}
