/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.midterm02;

/**
 *
 * @author ripgg
 */
public class Sunglasses extends Glasses{
    
    public Sunglasses(String name, String shape, String feature, String model, char choose) {
        super(name, shape, feature, model, choose);
    }
    
    @Override
    public int setG(String name, String shape, String feature, String model, char choose) {
        if (choose == '1') {
            this.name = getN();
            this.shape = getS();
            this.feature = getF();
            this.model = model;
            
        } else if (choose == '0') {
            this.name = name;
            this.shape = shape;
            this.feature = feature;
            this.model = model;
        } 
        
        
        switch (model) {
            case "B":
                efficacy = 60;
                break;
            case "C":
                efficacy=70;
                break;
            case "D":
                efficacy=80;
                break;
            case "E":
                efficacy=90;
                break;
            default:
                return efficacy;
        }
        return efficacy;
    }
    
    @Override
    public int price(String shape){
        if(shape=="Round"){
            price+=100;
        }else if(shape=="Rectangle"){
            price+=125;
        }else if(shape=="Square"){
            price+=125;
        }else if(shape=="Sport"){
            price+=150;
        }else if(shape=="Pilot"){
            price+=100;
        }
        return price;
    }
}
